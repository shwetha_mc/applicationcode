#include <sys/ucontext.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#include "evpath.h"
//#include "dfg.h"
#include "ev_dfg.h"
//#include "../../dfg/config.h"
//#include "../../dfg/dfg_setup.h"
#define PROT_SIZE 4

static char *mastercontact;
typedef struct _mon_app_data {
        int rangebins;
        int oversample;
} mon_app_data, *mon_app_ptr;

CManager cm;
char nodename[] = "cnode";
//static EVdfg dfg;
static FMField mon_app_data_field_list[] =
{
        {"rangebins","integer",sizeof(int), FMOffset(mon_app_ptr, rangebins)},
         {"oversample","integer",sizeof(int), FMOffset(mon_app_ptr, oversample)},
         {NULL, NULL, 0, 0}
};

static FMStructDescRec mon_app_spy_format_list[] =
{
        {"mon_app_data",mon_app_data_field_list,sizeof(mon_app_data),NULL},
        {NULL, NULL}
};



static int proti=0;

static int prot_arr[]={0,1,2,4};
static char datatype;

int setdtype(char dtype)
{
    datatype=dtype;
}

static void
handler(int sig, siginfo_t *si, void *unused)
{
    printf("%c\n",datatype );
           printf("Got SIGSEGV at address: 0x%lx\n", (long) si->si_addr);
	//char nodenamea[]="cnode";
	char sourcenamea[]="src_a_spy_cnode";
	printf("Mastercontact %s nodename %s App_spy\n",mastercontact, nodename);
    switch(datatype)
    {
        case 'i': proti=(proti+1)%PROT_SIZE; 
                  mprotect(si->si_addr, sizeof(int),PROT_EXEC);
		  int *rb;
		  rb=(int*)si->si_addr;
/*		  cm=CManager_create();
		  EVclient test_client;
		  EVclient_sources source_capabilities;
	          //dfg = EVdfg_create(cm);
		  EVsource source;
		  //sourcenamea="src_a_spy";
		  //nodenamea="app_spy_cnode";
 		  source = EVcreate_submit_handle(cm,DFG_SOURCE,mon_app_spy_format_list);         
		  source_capabilities=EVclient_register_source(sourcenamea, source);
		  test_client = EVclient_assoc(cm,nodename,mastercontact,source_capabilities,NULL);
		  EVclient_ready_wait(test_client);
		  mon_app_data dat;
		  dat.rangebins=*rb;
		  dat.oversample=8;
		  if(EVclient_source_active(source))
			EVsubmit(source,&dat,NULL);
		  EVfree_source(source);
		  EVclient_ready_for_shutdown(test_client);
		  EVclient_wait_for_shutdown(test_client);*/
		  printf("App spy ends, %d %d\n",__LINE__,*rb);
		  break;
        default:   proti=(proti+1)%PROT_SIZE; 
                  mprotect(si->si_addr, sizeof(double),prot_arr[proti]);
                  break;

    }
}


void app_setup(char dtype)
{
    struct sigaction sa;
    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = handler;
    setdtype(dtype);
    sigaction(SIGSEGV, &sa, NULL);
}

