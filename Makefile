CC=gcc
EVPATH_ROOT=/home/shwetha_mc
DFG_ROOT=/home/shwetha_mc/dfg/
HDRFILES=include/init.h include/setup.h
LDFLAGS=-lm -lrt -lpthread 
CFLAGS=-std=c99
HEADERS= $(HDRFILES)
SOURCEDIR=src/
BINDIR=bin/
LIBRARIES=-L/home/shwetha_mc/lib -levpath -lcercs_env -latl -lffs -ldill -lpthread
#LIBS= -levpath -lcercs_env -latl -lffs -ldill -lpthread
HEADERIMP=src/init.c src/setup.c
BNODE=bnode
CNODE=covariance_rcu
INCS=-I. -I$(EVPATH_ROOT)/include -I$(DFG_ROOT)

all: $(CNODE) $(BNODE)

$(BNODE): 
	$(CC) -o $(BINDIR)$@ $(SOURCEDIR)$@.c $(HEADERS) $(HEADERIMP) $(INCS) $(LDFLAGS) $(LIBRARIES)

$(CNODE):
	$(CC) -o $(BINDIR)$@ $(SOURCEDIR)$@.c $(HEADERS) $(HEADERIMP) $(INCS) $(LDFLAGS) $(LIBRARIES)

.PHONY: clean

clean:
	rm -f bin/bnode bin/covariance bin/setup bin/init .o *~
