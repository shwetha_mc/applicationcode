#ifndef HEADER_FILE
#define HEADER_FILE
#include<complex.h>
#define NX 16
#define NY 16

double complex mul_comp(double complex a, double complex b);
double complex div_comp(double complex a, double complex b);
void vectorize(double complex *jnv, double complex jn[NX][NY]);
void inverse(double complex *Rinv);
void calc_covariance(double complex *Rinv, double complex *R);

#endif
